<?php

use Illuminate\Http\Request;
Use App\Pelicula;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Peliculas
 
Route::get('peliculas', 'PeliculaController@index');
 
Route::get('peliculas/{id}', 'PeliculaController@show');

Route::get('recientes/peliculas', 'PeliculaController@recientes');

Route::post('peliculas', 'PeliculaController@store');

Route::put('peliculas/{id}', 'PeliculaController@store');

Route::delete('peliculas/{id}', 'PeliculaController@destroy');

Route::post('peliculas/search', 'PeliculaController@filter');

Route::get('valoracion/peliculas', 'ValoracionPeliculaController@index_valoracion');

Route::post('peliculas/{id}/valoracion', 'ValoracionPeliculaController@store');

//Series

Route::apiResources([

    'series'=>'SerieController',
    
]);

Route::put('series/{id}', 'SerieController@store');

Route::get('recientes/series', 'SerieController@recientes');

Route::post('series/search', 'SerieController@filter');

Route::get('valoracion/series', 'ValoracionSerieController@index_valoracion');

Route::post('series/{id}/valoracion', 'ValoracionSerieController@store');
