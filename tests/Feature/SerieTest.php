<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SerieTest extends TestCase
{
    /**
     * @test
     * @return void
     */

     
    public function test_listar_SerieTest()
    {
        $response = $this->get('/api/series');

        $response->assertStatus(200);
    }


    public function test_agregar_SerieTest()
    {
        $response = $this->json('POST', '/api/series', ['titulo' => 'Sally',
                                                            'descripcion' => 'Prueba',
                                                            'estreno'=>'2015-02-02',
                                                            'categoria'=>8,
                                                          ]);

        $response->assertStatus(201);
    }

    public function test_editar_SerieTest()
    {
        $response = $this->json('PUT', '/api/series/{id}', ['id' => 1,
                                                            'titulo' => 'Sally',
                                                            'descripcion' => 'Prueba',
                                                            'estreno'=>'2015-02-02',
                                                            'categoria'=>8,
                                                          ]);

        $response->assertStatus(200);

    }

    
    public function test_eliminar_SerieTest()
    {
        $response = $this->delete('/api/series/15');

        $response->assertStatus(200);

    }

    public function test_search_SerieTest()
    {
        $response = $this->json('POST', '/api/series/search', ['titulo' => 'Qui',
                                                                'categoria' => 2]);
        
        $response->assertStatus(200);
        
    }

    public function test_valoracion_SerieTest()
    {
        $response = $this->get('/api/valoracion/series');
        
        $response->assertStatus(200);
        
    }

    public function test_recientes_SerieTest()
    {
        $response = $this->get('/api/recientes/series');
        
        $response->assertStatus(200);
        
    }

    public function test_valorar_SerieTest()
    {
        $response = $this->json('POST', '/api/series/{id}/valoracion', ['id_serie' => 10,
                                                                'valoracion' => 2]);
        
        $response->assertStatus(201);
        
    }

}
