<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PeliculaTest extends TestCase
{
    
    public function test_listar_PeliculaTest()
    {
        $response = $this->get('/api/peliculas');

        $response->assertStatus(200);
    }


    public function test_agregar_PeliculaTest()
    {
        $response = $this->json('POST', '/api/peliculas', ['titulo' => 'Sally',
                                                            'descripcion' => 'Prueba',
                                                            'published'=>'2015-02-02',
                                                            'categoria'=>8,
                                                          ]);

        $response->assertStatus(201);
    }

    public function test_editar_PeliculaTest()
    {
        $response = $this->json('PUT', '/api/peliculas/{id}', ['id' => 21,
                                                            'titulo' => 'Sally',
                                                            'descripcion' => 'Prueba',
                                                            'published'=>'2015-02-02',
                                                            'categoria'=>8,
                                                          ]);

        $response->assertStatus(200);

    }

    
    public function test_eliminar_PeliculaTest()
    {
        $response = $this->delete('/api/peliculas/44');

        $response->assertStatus(200);

    }

    public function test_search_PeliculaTest()
    {
        $response = $this->json('POST', '/api/peliculas/search', ['titulo' => 'sunt',
                                                                'categoria' => 2]);
        
        $response->assertStatus(200);
        
    }

    public function test_valoracion_PeliculaTest()
    {
        $response = $this->get('/api/valoracion/peliculas');
        
        $response->assertStatus(200);
        
    }

    public function test_recientes_PeliculaTest()
    {
        $response = $this->get('/api/recientes/peliculas');
        
        $response->assertStatus(200);
        
    }

    public function test_valorar_PeliculaTest()
    {
        $response = $this->json('POST', '/api/peliculas/{id}/valoracion', ['id_pelicula' => 22,
                                                                'valoracion' => 2]);
        
        $response->assertStatus(201);
        
    }


}
