<?php

use Faker\Generator as Faker;

$factory->define(App\Serie::class, function (Faker $faker) {
    return [
        'titulo' => $faker->sentence(2),
        'descripcion' => $faker->text($maxNbChars = 140),
        'estreno' => $faker->year($max = 'now') ,
        'categoria' => $faker->numberBetween($min = 1, $max = 10), 
        'calificacion' => $faker->numberBetween($min = 1, $max = 5),
    ];
});
