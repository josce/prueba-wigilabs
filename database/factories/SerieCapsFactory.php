<?php

use Faker\Generator as Faker;

$factory->define(App\SerieCap::class, function (Faker $faker) {
    return [
        'id_serie' => function(array $serie) {
            return App\Serie::find($serie['id'])->first;
            },
        'temporada' => $faker->numberBetween($min=1, $max=5),
        'capitulo' => $faker->numberBetween($min=1, $max=10),
        'published' => $faker->date('Y-m-d',$max='now'), 
        'titulo' => $faker->sentence(2),
    ];
});
