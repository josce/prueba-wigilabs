<?php

use Faker\Generator as Faker;

$factory->define(App\ValoracionPelicula::class, function (Faker $faker) {
    return [
        'valoracion'=>$faker->numberBetween($min=1, $max=5),
    ];
});

