<?php

use Faker\Generator as Faker;

$factory->define(App\Pelicula::class, function (Faker $faker) {
    return [
        'titulo' => $faker->sentence(2),
        'descripcion' => $faker->paragraph,
        'published' => $faker->date,
        'categoria' => $faker->numberBetween($min = 1, $max = 10), 
        'calificacion' => $faker->numberBetween($min = 1, $max = 5),
    ];
});
