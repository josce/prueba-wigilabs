<?php

use Illuminate\Database\Seeder;
use App\SerieCap;

class SeriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Serie::class, 20)->create();
    }
}
