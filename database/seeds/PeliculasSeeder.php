<?php

use Illuminate\Database\Seeder;
use App\Pelicula;

class PeliculasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Pelicula::class, 20)->create();
    }
}
