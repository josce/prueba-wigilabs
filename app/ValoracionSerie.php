<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Serie;

class ValoracionSerie extends Model
{
    protected $fillable = ['id_serie', 'valoracion'];

    public $timestamps = false;

    public function serie()
    {
      return $this->belongsTo(Serie::class);
    }
}
