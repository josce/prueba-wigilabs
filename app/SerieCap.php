<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Serie;

class SerieCap extends Model
{
    protected $fillable = ['id_serie', 'temporada', 'capitulo', 'published', 'titulo'];

    public $timestamps = false;

    public function serie()
    {
      return $this->belongsTo(Serie::class,'id_serie','id');
    }
}
