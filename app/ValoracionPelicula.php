<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pelicula;

class ValoracionPelicula extends Model
{
    protected $fillable = ['id_pelicula', 'valoracion'];

    public $timestamps = false;

    public function pelicula()
    {
      return $this->belongsTo(Pelicula::class, 'id_pelicula', 'id');
    }
}
