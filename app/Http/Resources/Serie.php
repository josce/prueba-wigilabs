<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class Serie extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
        'id'=>$this->id,
        'titulo'=>$this->titulo,
        'descripcion'=>$this->descripcion,
        'estreno'=>$this->estreno,
        'categoria'=>$this->categoria,
        'calificacion'=>$this->valoracion_serie->avg('valoracion'),
        'capitulo'=>$this->serie_cap('capitulo')->get(),
        ];       
        
    }
}
