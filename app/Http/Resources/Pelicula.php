<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Pelicula extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       //return parent::toArray($request);
      return[
           'id'=>$this->id,
           'titulo'=>$this->titulo,
           'descripcion'=>$this->descripcion,
           'published'=>$this->published,
           'categoria'=>$this->categoria,
           'calificacion'=>$this->valoracion_pelicula->avg('valoracion'),
       ];
    }
}
