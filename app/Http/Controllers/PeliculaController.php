<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Pelicula as PeliculaResource;

class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$peliculas = Pelicula::with('valoracion_pelicula')->paginate(20);
        $peliculas = Pelicula::paginate(20);
        return PeliculaResource::collection($peliculas);
    }

    public function store(Request $request)
    {
        $pelicula = $request->isMethod('put') ? 
                    Pelicula::findOrFail($request->id) :
                    new Pelicula;

        $pelicula->id = $request->input('id');
        $pelicula->titulo = $request->input('titulo');
        $pelicula->descripcion = $request->input('descripcion');
        $pelicula->published = $request->input('published');
        $pelicula->categoria = $request->input('categoria');

        if($pelicula->save()) {
            return new PeliculaResource($pelicula);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pelicula = Pelicula::findOrFail($id);

        return new PeliculaResource($pelicula);
    }

    public function destroy($id)
    {
        
        $pelicula = Pelicula::findOrFail($id);

        if($pelicula->delete()){
            return new PeliculaResource($pelicula);
        }
    
    }

    public function filter(Request $request, Pelicula $pelicula)
    {
        if ($request->has('titulo')) {
          
            if ($request->has('categoria')) {
                $filtro=$pelicula->where('titulo', 'like', '%'. $request->input('titulo').'%')
                ->where('categoria', $request->input('categoria'))
                ->get();

                return PeliculaResource::collection($filtro);

            }  
            
            $filtro=$pelicula->where('titulo', 'like', '%'. $request->input('titulo').'%')->get();
            return PeliculaResource::collection($filtro);

        }

        if ($request->has('categoria')) {
            $filtro=$pelicula->where('categoria', $request->input('categoria'))->get();
            return PeliculaResource::collection($filtro);
        }
    }

    public function recientes()
    {
        $recientes=Pelicula::paginate(20)->sortBy('published');
        return PeliculaResource::collection($recientes);
    }


}
