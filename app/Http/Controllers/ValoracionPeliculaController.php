<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\ValoracionPelicula;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ValoracionPelicula as ValoracionPeliculaResource;

class ValoracionPeliculaController extends Controller
{
    public function index_valoracion()
    {
    
      $valoracion=DB::table('valoracion_peliculas')
                      ->select(DB::raw('id_pelicula, AVG(valoracion) as avg'))
                      ->groupBy('id_pelicula')
                      ->orderBy('avg', 'desc')
                      ->get();

     // var_dump($valoracion);
      return ValoracionPeliculaResource::collection($valoracion);

    }

    public function store(Request $request, Pelicula $pelicula)
    {
      $valoracion = ValoracionPelicula::create(
        [
          'id_pelicula' => $request->id_pelicula,
          'valoracion' => $request->valoracion
        ]
      );

      return new ValoracionPeliculaResource($valoracion);

    }


}
