<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Serie;
use App\SerieCap;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Serie as SerieResource;

class SerieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $series = Serie::with('serie_cap')->paginate(20);
        return SerieResource::collection($series);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serie = $request->isMethod('put') ? 
        Serie::findOrFail($request->id) :
        new Serie;

        $serie->id = $request->input('id');
        $serie->titulo = $request->input('titulo');
        $serie->descripcion = $request->input('descripcion');
        $serie->estreno = $request->input('estreno');
        $serie->categoria = $request->input('categoria');

        if($serie->save()) {
        return new SerieResource($serie);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $serie=Serie::findOrFail($request->id);
        
        $serie->id = $request->input('id');
        $serie->titulo = $request->input('titulo');
        $serie->descripcion = $request->input('descripcion');
        $serie->estreno = $request->input('estreno');
        $serie->categoria = $request->input('categoria');
       
        if($serie->save()) {
            return new SerieResource($serie);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $serie = Serie::findOrFail($id);

        if($serie->delete()){
            return new SerieResource($serie);
        }
    }

    public function filter(Request $request, Serie $serie)
    {
        if ($request->has('titulo')) {
          
            if ($request->has('categoria')) {
                $filtro=$serie->where('titulo', 'like', '%'. $request->input('titulo').'%')
                ->where('categoria', $request->input('categoria'))
                ->get();

                return SerieResource::collection($filtro);

            }  
            
            $filtro=$serie->where('titulo', 'like', '%'. $request->input('titulo').'%')->get();
            return SerieResource::collection($filtro);

        }

        if ($request->has('categoria')) {
            $filtro=$serie->where('categoria', $request->input('categoria'))->get();
            return SerieResource::collection($filtro);
        }
    }

    public function recientes()
    {
        $recientes=Serie::paginate(20)->sortByDesc('estreno');
        return SerieResource::collection($recientes);
    }

}
