<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Serie;
use App\ValoracionSerie;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ValoracionSerie as ValoracionSerieResource;

class ValoracionSerieController extends Controller
{
    public function index_valoracion()
    {
    
      $valoracion=DB::table('valoracion_series')
                      ->select(DB::raw('id_serie, AVG(valoracion) as avg'))
                      ->groupBy('id_serie')
                      ->orderBy('avg', 'desc')
                      ->get();

     // var_dump($valoracion);
      return ValoracionSerieResource::collection($valoracion);

    }

    public function store(Request $request, Serie $serie)
    {
      $valoracion = ValoracionSerie::create(
        [
          'id_serie' => $request->id_serie,
          'valoracion' => $request->valoracion
        ]
      );

      return new ValoracionSerieResource($valoracion);

    }
   
}
