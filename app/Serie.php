<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria;
use App\SerieCap;
use App\ValoracionSerie;

class Serie extends Model
{
    protected $fillable = ['titulo', 'descripcion', 'estreno', 'calificacion', 'categoria'];

    protected $visible = ['id'];

    public $timestamps = false;

    public function categoria()
    {
      return $this->belongsTo(Categoria::class,'id_categoria','id');
    }

    public function serie_cap()
    {
      return $this->hasMany(SerieCap::class,'id_serie','id');
    }

    public function valoracion_serie()
    {
      return $this->hasMany(ValoracionSerie::class,'id_serie','id');
    }
}
