<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;
use App\Categoria;
use App\ValoracionPelicula;

class Pelicula extends Model
{

    /**
     * Automatically persist the model in the Firebase realtime
     * database, whenever it gets created/updated/deleted
     */
    use SyncsWithFirebase;

    protected $fillable = ['titulo', 'descripcion', 'published', 'calificacion', 'categoria'];

    protected $visible = ['id'];

    public $timestamps = false;

    public function categoria()
    {
      return $this->belongsTo(Categoria::class,'id_categoria','id');
    }

    public function valoracion_pelicula()
    {
      return $this->hasMany(ValoracionPelicula::class,'id_pelicula','id');
    }

}

