<?php
$lista_peli=file_get_contents('http://localhost:8000/api/peliculas');
$peliculas=json_decode($lista_peli,true);
?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8"/>
        <title>Peliculas</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

         <div class="busqueda">

            <form action="search.php" method="POST">
                <input class="buscar" type="text" id="titulo" name="titulo"></input>
                <button>Buscar</button>
            </form>

        </div>
        <select>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
        </select>
    <div class="container">
            
        <div class="pagina">
            <h1>Películas</h1>
            
        </div>
  
                <?php foreach($peliculas as $pelicula => $cpelicula){?>
                    <?php foreach($cpelicula as $key => $row){?>
                        <div class="afuera">
                            <div class="box">
                                <div class="titulo">
                                    <h2><?php echo $row['titulo']?></h2>
                                </div>
                                <div class="descripcion">
                                    <h3>Descripcion: <?php echo $row['descripcion']?></h3>
                                    <div class="fecha">
                                        <h5>Fecha: <?php echo $row['published']?></h5>
                                    </div>
                                </div>
                                <div class="categoria">
                                    <h5>Categoria: <?php echo $row['categoria']?></h5>
                                </div>
                                <div id="calificacion">
                                    <h5>Calificación: <?php echo $row['calificacion']?></h5>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
            <?php } ?>
     
            
        </div>
    </body>