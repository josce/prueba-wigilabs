-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-07-2018 a las 19:24:34
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `peliculas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `descripcion`) VALUES
(1, 'Molestias esse alias dolor autem nobis quidem.'),
(2, 'Ut impedit nemo ut.'),
(3, 'Est unde ipsa voluptatem repellendus.'),
(4, 'Vel et vero.'),
(5, 'Nobis voluptatum assumenda.'),
(6, 'Aut officiis.'),
(7, 'Quia aut eius.'),
(8, 'Doloribus amet.'),
(9, 'Unde id.'),
(10, 'Autem autem quaerat.'),
(11, 'Laborum quae omnis.'),
(12, 'Voluptatem ducimus.'),
(13, 'Sapiente tempora beatae.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas`
--

CREATE TABLE `peliculas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `published` date NOT NULL,
  `categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `peliculas`
--

INSERT INTO `peliculas` (`id`, `titulo`, `descripcion`, `published`, `categoria`) VALUES
(21, 'Sally', 'Prueba', '2015-02-02', 8),
(22, 'Sunt doloremque.', 'Rerum eligendi dolor adipisci. Et vel architecto et tenetur aliquid illo voluptatem.', '2001-01-02', 3),
(23, 'Iure quo excepturi.', 'Magni ex cumque quaerat nisi. Aut voluptatibus error aliquid id nihil aut. Dolorum dolorum similique consectetur nobis odit alias cum. Praesentium harum optio est eligendi. Quidem quis est ullam enim.', '1976-11-21', 9),
(24, 'Provident ut voluptatem.', 'Vel velit est cupiditate debitis est. Omnis architecto velit quo rerum. A et distinctio maiores dolores.', '2015-02-02', 2),
(25, 'Ducimus totam reprehenderit.', 'Ea quis adipisci maiores. Deserunt natus sit ea et magnam. Fuga vel explicabo voluptas fugiat quia.', '1971-03-19', 1),
(26, 'Deleniti est ratione.', 'Incidunt ut est vitae ut ea sapiente. Sed sequi perferendis assumenda illum. Fugit numquam neque quo occaecati et aut aut est.', '2017-03-29', 8),
(27, 'Est quaerat beatae.', 'Ratione vitae necessitatibus quasi maiores omnis iure. Sunt autem culpa fugiat ab. Non nam eius laudantium.', '1998-11-03', 8),
(28, 'Consectetur rerum neque.', 'Quos sunt dicta dolor qui quia. Consequuntur eum est eos est autem sit iusto. Rerum deleniti nemo accusantium vitae quibusdam voluptatibus.', '2018-01-23', 2),
(29, 'Non aut.', 'Velit iusto est veniam. Distinctio nobis illo ea.', '1974-06-02', 7),
(30, 'Harum dolorem.', 'Iusto est iure magnam repellendus ea vitae. Officiis eum voluptas optio quia. Ullam totam rerum qui officia.', '1979-03-06', 4),
(31, 'Maiores rerum aut.', 'Sunt nesciunt inventore praesentium veritatis ut aliquam. Velit tenetur atque quasi aut. Et quos consectetur dolores maiores nobis architecto veniam.', '1972-05-12', 1),
(32, 'Ullam ut.', 'Consequuntur saepe eveniet ea eius excepturi ut sapiente. Quis incidunt iste tempora odio repudiandae. Aut excepturi consequatur quidem aliquid. Veniam nulla voluptatem autem eum qui quia.', '1970-03-02', 6),
(33, 'Beatae dignissimos.', 'Aut voluptatem distinctio aut numquam et esse. Sint est et natus eius minima vitae. Ab iure quibusdam sunt vel.', '2013-08-15', 2),
(34, 'Rerum harum.', 'Reprehenderit impedit quia eius. Culpa explicabo cum at consequatur consequuntur veritatis. Nam similique et impedit aut inventore eius optio rerum. Aliquid fugiat quis qui architecto voluptatibus.', '1991-04-23', 3),
(35, 'Quia rerum qui.', 'Voluptates fugiat voluptatem qui aliquam et velit delectus. Libero eveniet odit est ullam iure possimus id. Vel iste est expedita dolorem dolorum exercitationem. Nostrum ab ut blanditiis totam deserunt sit earum.', '2000-09-20', 1),
(36, 'Quia quis itaque.', 'Sed iure et in ex pariatur voluptatibus sunt omnis. Doloremque laborum eum et. Aut qui dolore et. At rerum ipsum reiciendis iure nihil.', '1984-07-19', 4),
(37, 'Corporis id distinctio.', 'Voluptatibus aut placeat provident sed est est. Cupiditate occaecati asperiores nihil in qui quasi incidunt. Accusamus sit voluptas non molestiae.', '1991-12-22', 2),
(38, 'Quidem qui laudantium.', 'Qui commodi est quia vel odio a vitae. Et repudiandae reprehenderit distinctio rerum. Placeat nisi repellendus molestiae.', '2000-05-25', 9),
(39, 'Sed exercitationem.', 'Omnis voluptatem ut esse velit dolor sunt in. Nesciunt ea veniam beatae porro ipsam velit. Possimus pariatur repellendus fugiat dolorem aliquam velit est error. Id provident aspernatur nihil facilis.', '2001-01-20', 3),
(40, 'Sunt itaque asperiores.', 'Nihil iure accusamus rerum cum quisquam. Odio ut consequuntur reiciendis eum dolorem ut doloremque. Et tempora ut ea reiciendis consequatur.', '1987-03-29', 2),
(41, 'probando la api', 'estoy enviando esto desde postman', '2017-01-30', 5),
(42, 'actualizando desde la api', 'estoy enviando esto desde postman', '2017-01-30', 5),
(46, 'Sally', 'Prueba', '0000-00-00', 8),
(47, 'Sally', 'Prueba', '0000-00-00', 8),
(48, 'Sally', 'Prueba', '0000-00-00', 8),
(49, 'Sally', 'Prueba', '0000-00-00', 8),
(50, 'Sally', 'Prueba', '0000-00-00', 8),
(51, 'Sally', 'Prueba', '0000-00-00', 8),
(52, 'Sally', 'Prueba', '0000-00-00', 8),
(53, 'Sally', 'Prueba', '0000-00-00', 8),
(54, 'Sally', 'Prueba', '2015-02-02', 8),
(55, 'Sally', 'Prueba', '2015-02-02', 8),
(56, 'Sally', 'Prueba', '2015-02-02', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series`
--

CREATE TABLE `series` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` varchar(140) NOT NULL,
  `estreno` year(4) NOT NULL,
  `categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `series`
--

INSERT INTO `series` (`id`, `titulo`, `descripcion`, `estreno`, `categoria`) VALUES
(1, 'Sally', 'Prueba', 2015, 8),
(2, 'Distinctio sint.', 'Sit iure numquam ratione. Rerum nemo dolorem eligendi rerum. Nulla molestiae qui laudantium enim voluptatem. Delectus quam quia quia.', 1982, 7),
(3, 'Qui illo.', 'Animi quia tempore excepturi nobis. Dolor et vel sunt laborum. Voluptatem id officiis eligendi nesciunt non.', 1984, 6),
(4, 'Suscipit laborum.', 'Eos corrupti numquam sunt quo ipsum praesentium et. Eum numquam quia eum reiciendis tempora in. Non sit facere enim facere.', 2001, 5),
(5, 'Et est rerum.', 'Eos quasi nisi ducimus in. Fugit provident voluptas aut dolore consequatur.', 1995, 3),
(6, 'At quam.', 'Quos sed est eos architecto. Minus autem perspiciatis laborum rerum cumque sit voluptatem sunt. Repellat quam sunt amet dicta.', 1995, 2),
(7, 'Dolorum omnis ea.', 'Non ducimus quae deserunt. Eius nulla magni delectus facere aut ab inventore. Amet architecto nesciunt dolores ipsum sint eaque saepe.', 1973, 5),
(8, 'Molestiae molestias.', 'Et qui est cum sed eaque consectetur. Blanditiis repellendus necessitatibus et suscipit ab. Aut fuga culpa quisquam qui.', 1975, 7),
(9, 'Officia cumque laborum.', 'Eum incidunt ex alias dolor sed illo at. Voluptatum facere doloremque quae harum et laudantium.', 1990, 10),
(10, 'Doloribus velit.', 'Ex odio dolorem id adipisci fugiat autem sit aliquid. Ea vero eaque dicta. In impedit veritatis quasi est ut maxime.', 1978, 4),
(14, 'agregando desde postman', 'hola soy postman', 1998, 10),
(19, 'Sally', 'Prueba', 2015, 8),
(20, 'Sally', 'Prueba', 2015, 8),
(21, 'Sally', 'Prueba', 2015, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serie_caps`
--

CREATE TABLE `serie_caps` (
  `id_serie` int(11) NOT NULL,
  `temporada` int(11) NOT NULL,
  `capitulo` int(11) NOT NULL,
  `published` date NOT NULL,
  `titulo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `serie_caps`
--

INSERT INTO `serie_caps` (`id_serie`, `temporada`, `capitulo`, `published`, `titulo`) VALUES
(1, 1, 1, '1986-10-10', 'Et et quo.'),
(1, 1, 2, '2004-09-19', 'Ut ex animi.'),
(1, 1, 3, '1985-12-09', 'Non nulla corrupti.'),
(1, 1, 4, '1980-04-28', 'Sed asperiores rerum.'),
(1, 1, 5, '1980-02-07', 'Porro ipsam nihil.'),
(1, 1, 6, '1984-01-09', 'Eligendi molestiae.'),
(1, 1, 7, '1990-04-05', 'Exercitationem aut.'),
(1, 1, 8, '1997-04-12', 'Enim in qui.'),
(1, 1, 9, '1978-12-23', 'Inventore qui excepturi.'),
(1, 1, 10, '2008-02-29', 'Tempore dolore.'),
(1, 2, 1, '1996-07-15', 'Sequi minus sed.'),
(1, 2, 2, '1976-02-02', 'Quis vitae mollitia.'),
(1, 2, 3, '1999-04-22', 'Nihil totam laborum.'),
(1, 2, 4, '1975-05-20', 'Consequatur labore velit.'),
(1, 2, 5, '1978-06-16', 'Molestiae perspiciatis voluptate.'),
(1, 2, 6, '1996-02-16', 'Deserunt in iure.'),
(1, 2, 7, '1979-05-15', 'Itaque quibusdam dolores.'),
(1, 2, 8, '2011-08-09', 'Voluptatem earum exercitationem.'),
(1, 2, 9, '1984-12-20', 'Dolorum ipsa error.'),
(1, 2, 10, '1977-07-28', 'Nulla ut voluptatem.'),
(1, 3, 1, '2001-11-16', 'Et voluptates est.'),
(1, 3, 2, '1994-08-15', 'Et architecto.'),
(1, 3, 3, '2002-02-12', 'Asperiores iusto accusamus.'),
(1, 3, 4, '1989-11-27', 'Odit nulla.'),
(1, 3, 5, '2006-12-28', 'Et omnis.'),
(1, 3, 6, '1992-09-11', 'Nisi aut.'),
(1, 3, 7, '2000-02-23', 'Explicabo sunt.'),
(1, 3, 8, '2012-01-17', 'Est dolor blanditiis.'),
(1, 3, 9, '2005-08-28', 'Autem eos.'),
(1, 3, 10, '1977-06-18', 'Voluptates recusandae.'),
(1, 4, 1, '1978-10-18', 'Optio vero dolorem.'),
(1, 4, 2, '1974-05-22', 'Rerum voluptatem.'),
(1, 4, 3, '1973-08-20', 'Quis vero tempore.'),
(1, 4, 4, '2015-10-01', 'Cupiditate ad.'),
(1, 4, 5, '1989-04-19', 'Qui mollitia rerum.'),
(1, 4, 6, '2010-07-23', 'Est corrupti.'),
(1, 4, 7, '2017-12-02', 'Hic numquam.'),
(1, 4, 8, '2004-08-26', 'Vel sint nemo.'),
(1, 4, 9, '2007-10-24', 'Ab ullam.'),
(1, 4, 10, '1984-08-22', 'Tempora dolorum omnis.'),
(1, 5, 1, '1973-11-12', 'Architecto quasi.'),
(1, 5, 2, '1980-12-29', 'Saepe itaque.'),
(1, 5, 3, '2018-01-07', 'Quia sequi omnis.'),
(1, 5, 4, '1973-12-25', 'In ipsum.'),
(1, 5, 5, '1997-12-27', 'Natus culpa veniam.'),
(1, 5, 6, '1994-08-29', 'Aut est.'),
(1, 5, 7, '2000-11-25', 'Doloremque voluptatum at.'),
(1, 5, 8, '1993-10-28', 'Tempora dolorem.'),
(1, 5, 9, '1973-09-08', 'Voluptate non.'),
(1, 5, 10, '2014-03-03', 'Incidunt animi.'),
(2, 1, 1, '1986-01-21', 'Perspiciatis dolor.'),
(2, 1, 2, '1994-02-22', 'Vel a adipisci.'),
(2, 1, 3, '1972-07-24', 'Dolore iure quia.'),
(2, 1, 4, '2001-03-11', 'A doloremque blanditiis.'),
(2, 1, 5, '1994-04-30', 'Facilis adipisci pariatur.'),
(2, 1, 6, '1974-03-27', 'Vitae eius libero.'),
(2, 1, 7, '1997-12-19', 'Maxime aspernatur.'),
(2, 1, 8, '1980-02-07', 'Voluptatem nihil.'),
(2, 1, 9, '2013-09-01', 'Laborum odio.'),
(2, 1, 10, '1980-06-25', 'Et deleniti saepe.'),
(2, 2, 1, '2002-05-01', 'Libero eligendi sequi.'),
(2, 2, 2, '2014-09-18', 'Quo quaerat quas.'),
(2, 2, 3, '2017-04-11', 'Maiores dolor.'),
(2, 2, 4, '1978-03-13', 'Accusantium suscipit.'),
(2, 2, 5, '2013-05-21', 'Qui officia voluptatem.'),
(2, 2, 6, '2002-12-11', 'Dolores qui.'),
(2, 2, 7, '2018-05-02', 'Dolor autem.'),
(2, 2, 8, '1978-07-06', 'Dolore voluptatum illum.'),
(2, 2, 9, '2002-02-03', 'Aspernatur aliquid ut.'),
(2, 2, 10, '1975-07-17', 'Adipisci est.'),
(2, 3, 1, '2008-03-10', 'Error a et.'),
(2, 3, 2, '1990-10-24', 'Minus ipsa.'),
(2, 3, 3, '2007-11-19', 'Debitis et.'),
(2, 3, 4, '2001-01-07', 'Error sapiente.'),
(2, 3, 5, '2012-08-12', 'Fugit veritatis.'),
(2, 3, 6, '1995-11-19', 'Quos id numquam.'),
(2, 3, 7, '1998-11-26', 'Praesentium asperiores.'),
(2, 3, 8, '1971-09-25', 'In impedit tenetur.'),
(2, 3, 9, '1988-01-22', 'Et quod non.'),
(2, 3, 10, '1986-04-09', 'Corporis reiciendis.'),
(2, 4, 1, '2012-03-07', 'Voluptas iusto.'),
(2, 4, 2, '1971-05-23', 'Nisi architecto.'),
(2, 4, 3, '2017-05-24', 'Ipsam eum.'),
(2, 4, 4, '1979-07-24', 'Vitae aliquam.'),
(2, 4, 5, '1983-05-04', 'Quae esse.'),
(2, 4, 6, '2008-01-30', 'Esse qui.'),
(2, 4, 7, '1984-09-25', 'Fugiat dolores voluptatibus.'),
(2, 4, 8, '1990-11-20', 'Iste ullam sit.'),
(2, 4, 9, '1998-07-29', 'Qui accusantium.'),
(2, 4, 10, '1997-01-11', 'Aut dolor dolores.'),
(2, 5, 1, '2012-09-08', 'Quod quasi ut.'),
(2, 5, 2, '1992-05-19', 'Et vitae.'),
(2, 5, 3, '2012-06-02', 'Officia fugit.'),
(2, 5, 4, '1991-05-05', 'Laudantium reiciendis necessitatibus.'),
(2, 5, 5, '1973-10-02', 'Sit nam magnam.'),
(2, 5, 6, '1974-12-27', 'Impedit reiciendis.'),
(2, 5, 7, '2017-01-14', 'Eligendi necessitatibus accusantium.'),
(2, 5, 8, '1990-05-01', 'Id distinctio.'),
(2, 5, 9, '1971-12-20', 'Necessitatibus voluptate reprehenderit.'),
(2, 5, 10, '1980-05-16', 'Perspiciatis odit.'),
(3, 1, 1, '2005-05-01', 'Sunt quibusdam necessitatibus.'),
(3, 1, 2, '1977-05-22', 'Quae eveniet suscipit.'),
(3, 1, 3, '1982-11-30', 'Voluptate tenetur eos.'),
(3, 1, 4, '1979-12-15', 'Ratione rerum.'),
(3, 1, 5, '1987-06-29', 'Officiis inventore ea.'),
(3, 1, 6, '1999-09-02', 'Reiciendis optio dicta.'),
(3, 1, 7, '1980-05-23', 'Ullam aut.'),
(3, 1, 8, '1994-03-10', 'Sequi est natus.'),
(3, 1, 9, '2001-01-05', 'Vel labore totam.'),
(3, 1, 10, '1977-09-01', 'Voluptatem voluptatem sed.'),
(3, 2, 1, '2009-02-23', 'Sunt culpa nostrum.'),
(3, 2, 2, '1996-01-26', 'Eos non.'),
(3, 2, 3, '2007-07-13', 'Quo dolor.'),
(3, 2, 4, '1982-02-16', 'Odio nihil accusamus.'),
(3, 2, 5, '2007-09-05', 'Et expedita.'),
(3, 2, 6, '1988-06-08', 'Delectus tenetur.'),
(3, 2, 7, '2005-07-06', 'Illo officiis odio.'),
(3, 2, 8, '2015-01-02', 'Odio consequuntur.'),
(3, 2, 9, '2000-02-26', 'Nisi qui suscipit.'),
(3, 2, 10, '1986-08-18', 'Aut eum.'),
(3, 3, 1, '1989-05-02', 'Recusandae sequi nesciunt.'),
(3, 3, 2, '2011-11-14', 'Doloribus laudantium quae.'),
(3, 3, 3, '2011-03-25', 'Et aliquid ipsam.'),
(3, 3, 4, '1992-02-27', 'Modi quo.'),
(3, 3, 5, '1996-08-27', 'Quos voluptatem ducimus.'),
(3, 3, 6, '2010-06-25', 'Architecto rerum.'),
(3, 3, 7, '2003-08-25', 'In sapiente impedit.'),
(3, 3, 8, '1973-05-11', 'Distinctio aliquid cupiditate.'),
(3, 3, 9, '2017-06-29', 'Facere illum blanditiis.'),
(3, 3, 10, '2012-05-11', 'Est aliquam non.'),
(3, 4, 1, '1982-02-28', 'Excepturi eaque ducimus.'),
(3, 4, 2, '1982-03-16', 'Culpa cupiditate omnis.'),
(3, 4, 3, '2017-08-24', 'Quidem iusto qui.'),
(3, 4, 4, '1982-08-16', 'Iusto voluptates non.'),
(3, 4, 5, '1994-07-21', 'Fugiat deserunt occaecati.'),
(3, 4, 6, '1976-02-08', 'Eum sint voluptate.'),
(3, 4, 7, '1978-09-30', 'Error eaque eveniet.'),
(3, 4, 8, '2009-06-21', 'Dignissimos iusto.'),
(3, 4, 9, '2002-10-01', 'Eum quam sint.'),
(3, 4, 10, '1976-10-23', 'Error distinctio consequatur.'),
(3, 5, 1, '1995-07-09', 'Aut omnis consectetur.'),
(3, 5, 2, '1998-09-05', 'Sint laudantium.'),
(3, 5, 3, '1999-12-28', 'Ad est.'),
(3, 5, 4, '2009-07-22', 'Adipisci sit.'),
(3, 5, 5, '2013-05-01', 'Sequi corrupti.'),
(3, 5, 6, '1983-11-06', 'Vel tenetur temporibus.'),
(3, 5, 7, '1982-11-15', 'Totam ratione.'),
(3, 5, 8, '2004-05-14', 'Autem sed in.'),
(3, 5, 9, '2011-12-12', 'Recusandae dolore.'),
(3, 5, 10, '1985-02-02', 'Voluptatem consectetur.'),
(4, 1, 1, '2013-06-19', 'Nesciunt voluptatem.'),
(4, 1, 2, '2009-10-15', 'Ducimus et aut.'),
(4, 1, 3, '1992-08-30', 'Nostrum ratione tempora.'),
(4, 1, 4, '1984-01-04', 'Magni omnis iste.'),
(4, 1, 5, '2015-02-11', 'Qui necessitatibus.'),
(4, 1, 6, '2012-08-05', 'Autem vel.'),
(4, 1, 7, '2000-06-21', 'Odio natus.'),
(4, 1, 8, '2013-05-24', 'Quam maiores.'),
(4, 1, 9, '1980-04-12', 'Praesentium fugit quas.'),
(4, 1, 10, '1976-10-16', 'Quia suscipit ut.'),
(4, 2, 1, '1986-12-20', 'Dicta voluptate autem.'),
(4, 2, 2, '2002-07-16', 'Consequatur hic sunt.'),
(4, 2, 3, '1990-12-30', 'Iste voluptates.'),
(4, 2, 4, '2018-01-16', 'Cumque harum.'),
(4, 2, 5, '1980-03-27', 'Rerum quod.'),
(4, 2, 6, '2000-06-17', 'Veritatis sint.'),
(4, 2, 7, '2005-11-12', 'Facilis illum.'),
(4, 2, 8, '2001-04-23', 'Ab dolores soluta.'),
(4, 2, 9, '2013-06-10', 'Voluptatum magnam nihil.'),
(4, 2, 10, '2004-05-29', 'Aspernatur dolorem.'),
(4, 3, 1, '1998-09-16', 'Quo et.'),
(4, 3, 2, '1970-05-30', 'Qui dolor.'),
(4, 3, 3, '1987-05-29', 'Est non.'),
(4, 3, 4, '1999-01-29', 'Molestias a.'),
(4, 3, 5, '1982-09-05', 'Et officia amet.'),
(4, 3, 6, '1989-07-11', 'Aut beatae aut.'),
(4, 3, 7, '1978-10-15', 'Doloribus occaecati sit.'),
(4, 3, 8, '1976-07-23', 'Officia harum.'),
(4, 3, 9, '1989-11-13', 'Ipsam sed vel.'),
(4, 3, 10, '1971-03-09', 'Aliquid qui aut.'),
(4, 4, 1, '2017-05-25', 'Officia in.'),
(4, 4, 2, '2001-02-01', 'Vero alias non.'),
(4, 4, 3, '2014-07-31', 'Consequatur impedit.'),
(4, 4, 4, '1976-11-08', 'Eligendi et aut.'),
(4, 4, 5, '2016-03-05', 'Rerum vel.'),
(4, 4, 6, '2009-12-23', 'Cupiditate aliquid.'),
(4, 4, 7, '2015-04-19', 'Recusandae repellendus.'),
(4, 4, 8, '2000-01-02', 'Assumenda magnam.'),
(4, 4, 9, '2007-03-04', 'Voluptatem voluptatum.'),
(4, 4, 10, '1997-02-22', 'Non dolores.'),
(4, 5, 1, '1991-06-25', 'Expedita sunt laborum.'),
(4, 5, 2, '1979-03-28', 'Ipsa est.'),
(4, 5, 3, '1979-04-22', 'Delectus et.'),
(4, 5, 4, '1988-12-15', 'Ea doloremque ut.'),
(4, 5, 5, '1992-07-19', 'Dignissimos enim.'),
(4, 5, 6, '1978-11-13', 'Magnam consequuntur voluptatem.'),
(4, 5, 7, '2012-03-14', 'Laboriosam sit.'),
(4, 5, 8, '1979-12-13', 'Aspernatur sint.'),
(4, 5, 9, '1997-01-24', 'Autem perferendis.'),
(4, 5, 10, '1970-07-18', 'Ab consequatur.'),
(5, 1, 1, '1983-10-07', 'Repellat sed praesentium.'),
(5, 1, 2, '2009-04-01', 'Animi aut.'),
(5, 1, 3, '1987-01-29', 'Est illum possimus.'),
(5, 1, 4, '2018-02-28', 'Molestiae optio.'),
(5, 1, 5, '1991-03-05', 'Et quis omnis.'),
(5, 1, 6, '2008-03-02', 'Quia consequatur consequatur.'),
(5, 1, 7, '1983-05-03', 'Qui maiores.'),
(5, 1, 8, '2001-10-29', 'Eos deserunt enim.'),
(5, 1, 9, '1995-07-04', 'Minima voluptas.'),
(5, 1, 10, '1975-08-13', 'Ex et neque.'),
(5, 2, 1, '2001-04-15', 'Et et.'),
(5, 2, 2, '1972-04-14', 'Voluptatibus ut et.'),
(5, 2, 3, '2005-04-22', 'Et quia ullam.'),
(5, 2, 4, '1993-01-27', 'Sed molestiae.'),
(5, 2, 5, '1975-08-07', 'Omnis aut.'),
(5, 2, 6, '1978-01-03', 'Neque veniam.'),
(5, 2, 7, '2001-06-13', 'Adipisci iusto.'),
(5, 2, 8, '1984-05-08', 'Quaerat itaque.'),
(5, 2, 9, '1996-03-15', 'Dignissimos fuga.'),
(5, 2, 10, '2012-04-17', 'Autem tempore.'),
(5, 3, 1, '2010-06-02', 'Qui quidem unde.'),
(5, 3, 2, '2011-03-30', 'Repellat dignissimos.'),
(5, 3, 3, '1979-03-02', 'Occaecati provident.'),
(5, 3, 4, '2008-02-06', 'Aliquid voluptas a.'),
(5, 3, 5, '2003-11-17', 'Dolor ratione.'),
(5, 3, 6, '1986-08-05', 'Consectetur adipisci.'),
(5, 3, 7, '2016-04-07', 'Cum blanditiis.'),
(5, 3, 8, '1971-11-07', 'Minus omnis.'),
(5, 3, 9, '2008-01-23', 'Laboriosam occaecati at.'),
(5, 3, 10, '2012-10-22', 'Repellat vero.'),
(5, 4, 1, '1982-02-04', 'Rem unde distinctio.'),
(5, 4, 2, '1974-08-03', 'Consequatur mollitia.'),
(5, 4, 3, '1986-12-12', 'Quia error.'),
(5, 4, 4, '1997-02-04', 'Qui sint voluptatem.'),
(5, 4, 5, '1992-03-04', 'Sed libero molestias.'),
(5, 4, 6, '2017-09-29', 'Aut a.'),
(5, 4, 7, '1978-01-19', 'Sint ut omnis.'),
(5, 4, 8, '1978-10-22', 'Porro non molestiae.'),
(5, 4, 9, '2004-05-11', 'Facere velit.'),
(5, 4, 10, '2006-03-19', 'Omnis facilis aut.'),
(5, 5, 1, '2008-06-05', 'Asperiores voluptas.'),
(5, 5, 2, '2003-05-08', 'Doloremque officiis.'),
(5, 5, 3, '1996-08-25', 'Dolorum quis aut.'),
(5, 5, 4, '1990-04-30', 'Dolore qui et.'),
(5, 5, 5, '1974-05-18', 'Minima et.'),
(5, 5, 6, '2008-12-22', 'Aliquid ipsam.'),
(5, 5, 7, '1980-01-24', 'Ab ipsam.'),
(5, 5, 8, '2000-06-05', 'Qui itaque et.'),
(5, 5, 9, '1984-06-13', 'Possimus aperiam tempore.'),
(5, 5, 10, '2010-06-25', 'Laudantium omnis.'),
(6, 1, 1, '2011-04-14', 'Dicta nihil animi.'),
(6, 1, 2, '1987-11-28', 'Et assumenda cum.'),
(6, 1, 3, '1977-02-19', 'Error sed iure.'),
(6, 1, 4, '2017-09-22', 'Quaerat quod doloremque.'),
(6, 1, 5, '2018-01-31', 'Rerum nam.'),
(6, 1, 6, '1972-01-01', 'Sit laborum molestiae.'),
(6, 1, 7, '1973-05-03', 'Illo dolorem.'),
(6, 1, 8, '1974-09-05', 'Ullam qui.'),
(6, 1, 9, '2015-08-27', 'Quia doloribus.'),
(6, 1, 10, '1974-03-27', 'Voluptatibus cupiditate.'),
(6, 2, 1, '1984-04-23', 'Minus adipisci.'),
(6, 2, 2, '2008-08-16', 'Aliquid incidunt et.'),
(6, 2, 3, '1976-06-24', 'Incidunt voluptatibus eum.'),
(6, 2, 4, '2011-11-19', 'Et dicta.'),
(6, 2, 5, '1974-06-13', 'Quia eum.'),
(6, 2, 6, '1994-07-15', 'Sed neque.'),
(6, 2, 7, '1971-07-03', 'Rerum voluptatem delectus.'),
(6, 2, 8, '1974-02-22', 'Officia aliquam.'),
(6, 2, 9, '2000-06-28', 'Dolorem nam.'),
(6, 2, 10, '2007-10-16', 'Architecto molestiae dolores.'),
(6, 3, 1, '2003-09-13', 'Dignissimos autem dolores.'),
(6, 3, 2, '1994-06-02', 'Aliquam deleniti ex.'),
(6, 3, 3, '1990-12-27', 'Impedit quia fugiat.'),
(6, 3, 4, '2014-03-14', 'Ea repellat.'),
(6, 3, 5, '2003-01-20', 'Sapiente sit aut.'),
(6, 3, 6, '1970-12-20', 'Praesentium repudiandae.'),
(6, 3, 7, '2018-05-26', 'Et eligendi.'),
(6, 3, 8, '2010-03-01', 'Numquam ipsum qui.'),
(6, 3, 9, '1985-04-06', 'Vel ea.'),
(6, 3, 10, '1979-03-09', 'Inventore itaque voluptates.'),
(6, 4, 1, '1975-06-23', 'Et optio possimus.'),
(6, 4, 2, '2014-10-22', 'Id sit vitae.'),
(6, 4, 3, '1975-06-08', 'Quia et praesentium.'),
(6, 4, 4, '2011-03-15', 'Sed explicabo.'),
(6, 4, 5, '1988-11-20', 'In expedita quisquam.'),
(6, 4, 6, '1993-08-07', 'Dolorem eveniet odio.'),
(6, 4, 7, '2006-04-04', 'Accusantium nihil.'),
(6, 4, 8, '1991-11-23', 'Rerum dolore saepe.'),
(6, 4, 9, '2007-10-01', 'Qui eos.'),
(6, 4, 10, '1980-04-27', 'Et quis magnam.'),
(6, 5, 1, '2005-11-17', 'Nemo deleniti quod.'),
(6, 5, 2, '1997-05-06', 'Neque quidem.'),
(6, 5, 3, '2017-04-02', 'Reprehenderit ea.'),
(6, 5, 4, '2011-01-31', 'Quo ipsa est.'),
(6, 5, 5, '1991-10-02', 'Est non magnam.'),
(6, 5, 6, '2014-11-20', 'Temporibus unde.'),
(6, 5, 7, '1988-05-15', 'Repellendus totam.'),
(6, 5, 8, '1983-08-07', 'Eos ut nesciunt.'),
(6, 5, 9, '2016-01-09', 'Illo quo illum.'),
(6, 5, 10, '1991-05-08', 'Cumque omnis ut.'),
(7, 1, 1, '1992-06-05', 'Maxime maxime.'),
(7, 1, 2, '1972-05-03', 'Adipisci explicabo molestias.'),
(7, 1, 3, '2002-01-11', 'Qui sed error.'),
(7, 1, 4, '2005-07-16', 'Quia natus.'),
(7, 1, 5, '2012-02-29', 'Adipisci ea.'),
(7, 1, 6, '1974-05-18', 'Voluptatibus laudantium.'),
(7, 1, 7, '2017-09-21', 'Fuga incidunt qui.'),
(7, 1, 8, '1999-10-10', 'Qui et eum.'),
(7, 1, 9, '2012-07-19', 'Nobis delectus.'),
(7, 1, 10, '2003-11-23', 'Iure et a.'),
(7, 2, 1, '1972-05-27', 'Sequi et sed.'),
(7, 2, 2, '1970-03-26', 'Facilis sint quis.'),
(7, 2, 3, '2015-01-08', 'Non suscipit.'),
(7, 2, 4, '1993-01-02', 'Vel vitae et.'),
(7, 2, 5, '1986-05-05', 'Sunt quia voluptatem.'),
(7, 2, 6, '2010-02-14', 'Et sed expedita.'),
(7, 2, 7, '2008-07-16', 'Quos dolore.'),
(7, 2, 8, '2012-05-14', 'Sequi non eaque.'),
(7, 2, 9, '1999-05-07', 'Quisquam cupiditate.'),
(7, 2, 10, '1975-02-19', 'Architecto vel.'),
(7, 3, 1, '1985-02-06', 'Possimus quisquam non.'),
(7, 3, 2, '1987-12-23', 'Dolore sint omnis.'),
(7, 3, 3, '1972-09-21', 'Dolorum nobis sint.'),
(7, 3, 4, '1981-06-29', 'Quas tempore maiores.'),
(7, 3, 5, '1984-11-16', 'Dicta non ab.'),
(7, 3, 6, '1990-05-06', 'Voluptatem quos debitis.'),
(7, 3, 7, '2011-03-24', 'Quisquam consequatur debitis.'),
(7, 3, 8, '1975-08-23', 'Omnis delectus.'),
(7, 3, 9, '2004-07-25', 'Sint voluptatem.'),
(7, 3, 10, '2005-11-07', 'Libero minus eveniet.'),
(7, 4, 1, '2013-06-20', 'Perspiciatis qui doloremque.'),
(7, 4, 2, '2004-03-23', 'Facere et.'),
(7, 4, 3, '2011-01-05', 'Commodi provident.'),
(7, 4, 4, '1988-11-26', 'Reprehenderit ipsa.'),
(7, 4, 5, '1999-06-07', 'Blanditiis laudantium minima.'),
(7, 4, 6, '1972-07-02', 'Error officiis praesentium.'),
(7, 4, 7, '1997-05-13', 'Voluptatem alias laudantium.'),
(7, 4, 8, '2004-03-08', 'Voluptate quia.'),
(7, 4, 9, '1987-01-27', 'Mollitia et aliquam.'),
(7, 4, 10, '1974-12-14', 'Est et.'),
(7, 5, 1, '2009-02-07', 'Quo est doloremque.'),
(7, 5, 2, '1985-04-22', 'Et maxime.'),
(7, 5, 3, '2004-12-05', 'Eius facere.'),
(7, 5, 4, '1978-04-18', 'Harum illo.'),
(7, 5, 5, '1970-07-21', 'Nostrum et.'),
(7, 5, 6, '2008-06-01', 'Minima sed temporibus.'),
(7, 5, 7, '2002-03-26', 'Qui aut quod.'),
(7, 5, 8, '2016-08-16', 'Ut quae ipsam.'),
(7, 5, 9, '1973-07-10', 'Distinctio facere.'),
(7, 5, 10, '1997-12-22', 'Aspernatur eligendi.'),
(8, 1, 1, '1971-07-03', 'Magni consequuntur.'),
(8, 1, 2, '2014-03-16', 'Magni aperiam ut.'),
(8, 1, 3, '2004-04-23', 'Sit et qui.'),
(8, 1, 4, '1978-07-01', 'Tempora eveniet.'),
(8, 1, 5, '2006-10-21', 'Nam labore hic.'),
(8, 1, 6, '1994-01-21', 'Amet alias esse.'),
(8, 1, 7, '1986-03-17', 'Unde vitae et.'),
(8, 1, 8, '1985-07-10', 'Culpa ea.'),
(8, 1, 9, '2007-10-05', 'Nemo quaerat.'),
(8, 1, 10, '1997-05-08', 'Quo voluptatum.'),
(8, 2, 1, '1993-07-07', 'Voluptas nemo eligendi.'),
(8, 2, 2, '1990-06-22', 'Omnis voluptatem.'),
(8, 2, 3, '2012-11-22', 'Eius est.'),
(8, 2, 4, '1999-03-03', 'Laboriosam autem.'),
(8, 2, 5, '2004-12-24', 'Repellat iste.'),
(8, 2, 6, '2006-11-25', 'Non beatae.'),
(8, 2, 7, '2018-04-19', 'Natus a vitae.'),
(8, 2, 8, '2003-07-27', 'Illum error inventore.'),
(8, 2, 9, '2013-11-26', 'Commodi omnis officiis.'),
(8, 2, 10, '2002-11-29', 'Quibusdam in.'),
(8, 3, 1, '2008-07-28', 'Quod aut est.'),
(8, 3, 2, '1974-05-03', 'Nisi similique.'),
(8, 3, 3, '1982-09-27', 'Reiciendis qui.'),
(8, 3, 4, '1996-03-16', 'Nostrum nesciunt omnis.'),
(8, 3, 5, '1987-08-01', 'Sint atque.'),
(8, 3, 6, '1971-06-12', 'Eos dolorem.'),
(8, 3, 7, '2000-11-16', 'Assumenda exercitationem ut.'),
(8, 3, 8, '1980-04-16', 'Vitae totam.'),
(8, 3, 9, '1992-04-13', 'Nam animi.'),
(8, 3, 10, '1975-02-21', 'Fugit vitae.'),
(8, 4, 1, '1971-06-03', 'Ut autem asperiores.'),
(8, 4, 2, '1993-10-25', 'Voluptatum alias ut.'),
(8, 4, 3, '1993-11-08', 'Unde non.'),
(8, 4, 4, '1975-09-30', 'Temporibus nulla.'),
(8, 4, 5, '1974-01-16', 'Repellendus aut.'),
(8, 4, 6, '2009-02-19', 'Perferendis qui quia.'),
(8, 4, 7, '2001-12-13', 'Eum voluptas et.'),
(8, 4, 8, '1985-12-21', 'Nesciunt eligendi.'),
(8, 4, 9, '1995-03-18', 'Necessitatibus eum.'),
(8, 4, 10, '1987-07-01', 'Ab sunt.'),
(8, 5, 1, '2003-10-08', 'Voluptate qui.'),
(8, 5, 2, '2017-01-13', 'Iusto deleniti incidunt.'),
(8, 5, 3, '2001-09-23', 'Qui et laborum.'),
(8, 5, 4, '1982-10-20', 'Cupiditate consequatur.'),
(8, 5, 5, '1983-02-28', 'Natus sapiente velit.'),
(8, 5, 6, '1989-05-04', 'Enim rerum.'),
(8, 5, 7, '1972-12-28', 'Ea repellendus.'),
(8, 5, 8, '1993-07-05', 'Ut maiores aut.'),
(8, 5, 9, '1988-08-16', 'Aliquid accusantium nulla.'),
(8, 5, 10, '1985-11-09', 'Ad commodi magni.'),
(9, 1, 1, '2004-09-14', 'Perspiciatis iste id.'),
(9, 1, 2, '1972-07-21', 'Voluptatem sequi sed.'),
(9, 1, 3, '1990-11-18', 'Sed corrupti in.'),
(9, 1, 4, '1999-08-25', 'Eum itaque.'),
(9, 1, 5, '1973-02-24', 'Nesciunt at voluptatem.'),
(9, 1, 6, '1987-10-19', 'Adipisci et minus.'),
(9, 1, 7, '1977-06-19', 'Ipsa eveniet aut.'),
(9, 1, 8, '2011-04-06', 'Rerum vitae cumque.'),
(9, 1, 9, '2015-03-22', 'Voluptatibus ut.'),
(9, 1, 10, '2001-06-26', 'Necessitatibus repudiandae.'),
(9, 2, 1, '1970-12-31', 'Eius maiores.'),
(9, 2, 2, '1987-09-29', 'At sed quasi.'),
(9, 2, 3, '1986-10-24', 'Sapiente est.'),
(9, 2, 4, '1974-05-23', 'Voluptas aut consequuntur.'),
(9, 2, 5, '1998-01-12', 'Corporis ipsam.'),
(9, 2, 6, '2002-03-20', 'Optio cumque.'),
(9, 2, 7, '1999-07-04', 'Et et.'),
(9, 2, 8, '1995-05-14', 'Officia quo et.'),
(9, 2, 9, '2017-11-01', 'Qui ea qui.'),
(9, 2, 10, '1982-02-18', 'Asperiores iure impedit.'),
(9, 3, 1, '1970-02-16', 'Ut est.'),
(9, 3, 2, '1973-02-02', 'Incidunt ducimus.'),
(9, 3, 3, '1978-07-12', 'Et rem.'),
(9, 3, 4, '1992-05-16', 'Beatae distinctio.'),
(9, 3, 5, '2011-03-07', 'Quis non qui.'),
(9, 3, 6, '1997-01-14', 'Et quis.'),
(9, 3, 7, '2006-05-02', 'Reprehenderit est aperiam.'),
(9, 3, 8, '1989-07-29', 'Laudantium earum.'),
(9, 3, 9, '1995-07-01', 'Dolorem nisi aperiam.'),
(9, 3, 10, '1996-01-11', 'Eum pariatur sed.'),
(9, 4, 1, '1980-11-28', 'Aut fugiat.'),
(9, 4, 2, '2010-12-19', 'Maxime et officia.'),
(9, 4, 3, '1989-03-31', 'Est ut et.'),
(9, 4, 4, '1983-09-13', 'Dolor ipsam.'),
(9, 4, 5, '1977-04-24', 'Nostrum quo doloribus.'),
(9, 4, 6, '1982-06-19', 'Voluptatem consequatur.'),
(9, 4, 7, '1971-07-29', 'Dignissimos illum dolorem.'),
(9, 4, 8, '2002-05-23', 'Eos quo.'),
(9, 4, 9, '1998-11-30', 'Voluptas commodi.'),
(9, 4, 10, '1977-08-13', 'Qui sapiente assumenda.'),
(9, 5, 1, '1973-10-07', 'Eligendi asperiores.'),
(9, 5, 2, '1984-07-07', 'Ad quod suscipit.'),
(9, 5, 3, '2008-03-10', 'Quis sed.'),
(9, 5, 4, '2013-01-02', 'Itaque voluptas.'),
(9, 5, 5, '2015-02-15', 'Asperiores officia dolor.'),
(9, 5, 6, '1992-12-16', 'Sit minus.'),
(9, 5, 7, '1996-02-24', 'Quibusdam praesentium.'),
(9, 5, 8, '2005-02-10', 'Vel molestiae.'),
(9, 5, 9, '1972-12-04', 'Vero veniam quas.'),
(9, 5, 10, '2012-06-06', 'Et soluta.'),
(10, 1, 1, '2001-04-20', 'Sit cupiditate.'),
(10, 1, 2, '2001-03-12', 'Quis quod est.'),
(10, 1, 3, '1979-03-07', 'Facere voluptas.'),
(10, 1, 4, '2005-03-17', 'Aut harum.'),
(10, 1, 5, '2002-03-16', 'Eius dolorem.'),
(10, 1, 6, '1972-06-08', 'Eos unde sed.'),
(10, 1, 7, '1999-09-23', 'Aliquid est.'),
(10, 1, 8, '1998-06-25', 'Quisquam autem dolores.'),
(10, 1, 9, '1974-07-25', 'Libero natus.'),
(10, 1, 10, '1984-02-21', 'Ut facilis dolores.'),
(10, 2, 1, '2013-08-06', 'Id quod.'),
(10, 2, 2, '1989-01-06', 'Incidunt aliquid.'),
(10, 2, 3, '1982-10-15', 'Esse optio.'),
(10, 2, 4, '1970-09-08', 'Harum vel.'),
(10, 2, 5, '1982-11-16', 'Dolor excepturi.'),
(10, 2, 6, '1985-09-22', 'Ipsum et maiores.'),
(10, 2, 7, '1971-04-06', 'Dolorum eius provident.'),
(10, 2, 8, '1989-06-01', 'Ea quo officiis.'),
(10, 2, 9, '2000-10-01', 'Minima ipsum nemo.'),
(10, 2, 10, '2007-10-02', 'Et vitae sint.'),
(10, 3, 1, '1974-06-04', 'Dolorem molestias et.'),
(10, 3, 2, '1990-07-09', 'Quae consectetur possimus.'),
(10, 3, 3, '2015-01-20', 'Enim eum est.'),
(10, 3, 4, '1989-12-11', 'Rerum iste quia.'),
(10, 3, 5, '1988-11-10', 'Quas quaerat autem.'),
(10, 3, 6, '2017-11-13', 'Molestiae est.'),
(10, 3, 7, '2003-06-02', 'Adipisci sint.'),
(10, 3, 8, '1993-05-14', 'Aliquid corporis.'),
(10, 3, 9, '2011-12-28', 'Eligendi architecto.'),
(10, 3, 10, '1990-03-29', 'Sit ut.'),
(10, 4, 1, '1992-02-12', 'Voluptas aut quis.'),
(10, 4, 2, '1992-12-29', 'Sunt rerum.'),
(10, 4, 3, '1984-06-09', 'Amet et.'),
(10, 4, 4, '1995-08-18', 'Rerum non.'),
(10, 4, 5, '1982-07-06', 'Quibusdam facere reiciendis.'),
(10, 4, 6, '2010-12-25', 'Ad deserunt.'),
(10, 4, 7, '1977-07-20', 'Sunt eius.'),
(10, 4, 8, '2004-08-08', 'Eos et.'),
(10, 4, 9, '1972-02-04', 'Ipsa illum et.'),
(10, 4, 10, '1999-06-19', 'Doloremque occaecati aut.'),
(10, 5, 1, '1992-05-07', 'Qui aliquid qui.'),
(10, 5, 2, '2007-07-13', 'Harum sint ipsum.'),
(10, 5, 3, '1996-09-18', 'Eos recusandae.'),
(10, 5, 4, '1993-09-20', 'Deleniti non beatae.'),
(10, 5, 5, '2009-02-04', 'Praesentium quo ut.'),
(10, 5, 6, '2009-02-11', 'In et ipsum.'),
(10, 5, 7, '1971-03-29', 'Sapiente maxime.'),
(10, 5, 8, '1987-07-07', 'Quasi dignissimos.'),
(10, 5, 9, '2012-12-10', 'Assumenda mollitia.'),
(10, 5, 10, '2015-06-10', 'A deserunt.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valoracion_peliculas`
--

CREATE TABLE `valoracion_peliculas` (
  `id` int(11) NOT NULL,
  `id_pelicula` int(11) NOT NULL,
  `valoracion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `valoracion_peliculas`
--

INSERT INTO `valoracion_peliculas` (`id`, `id_pelicula`, `valoracion`) VALUES
(1, 21, 3),
(2, 22, 5),
(3, 23, 5),
(4, 24, 3),
(5, 25, 3),
(6, 26, 3),
(7, 27, 4),
(8, 28, 5),
(9, 29, 3),
(10, 30, 5),
(11, 31, 3),
(12, 32, 1),
(13, 33, 1),
(14, 34, 3),
(15, 35, 5),
(16, 36, 3),
(17, 37, 4),
(18, 38, 5),
(19, 39, 4),
(20, 40, 2),
(21, 21, 1),
(22, 21, 3),
(23, 21, 4),
(24, 22, 2),
(25, 22, 2),
(26, 22, 2),
(27, 22, 2),
(28, 22, 2),
(29, 22, 2),
(30, 22, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valoracion_series`
--

CREATE TABLE `valoracion_series` (
  `id` int(11) NOT NULL,
  `id_serie` int(11) NOT NULL,
  `valoracion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `valoracion_series`
--

INSERT INTO `valoracion_series` (`id`, `id_serie`, `valoracion`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 3),
(4, 4, 1),
(5, 5, 4),
(6, 6, 3),
(7, 7, 5),
(8, 8, 3),
(9, 9, 2),
(10, 10, 5),
(11, 1, 4),
(12, 1, 4),
(13, 3, 4),
(14, 2, 4),
(15, 10, 2),
(16, 10, 2),
(17, 10, 2),
(18, 10, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria` (`categoria`);

--
-- Indices de la tabla `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria` (`categoria`);

--
-- Indices de la tabla `serie_caps`
--
ALTER TABLE `serie_caps`
  ADD PRIMARY KEY (`id_serie`,`temporada`,`capitulo`);

--
-- Indices de la tabla `valoracion_peliculas`
--
ALTER TABLE `valoracion_peliculas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelicula` (`id_pelicula`);

--
-- Indices de la tabla `valoracion_series`
--
ALTER TABLE `valoracion_series`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_serie` (`id_serie`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `series`
--
ALTER TABLE `series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `valoracion_peliculas`
--
ALTER TABLE `valoracion_peliculas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `valoracion_series`
--
ALTER TABLE `valoracion_series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD CONSTRAINT `peliculas_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`id`);

--
-- Filtros para la tabla `series`
--
ALTER TABLE `series`
  ADD CONSTRAINT `series_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`id`);

--
-- Filtros para la tabla `serie_caps`
--
ALTER TABLE `serie_caps`
  ADD CONSTRAINT `serie_caps_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `series` (`id`);

--
-- Filtros para la tabla `valoracion_peliculas`
--
ALTER TABLE `valoracion_peliculas`
  ADD CONSTRAINT `valoracion_peliculas_ibfk_1` FOREIGN KEY (`id_pelicula`) REFERENCES `peliculas` (`id`);

--
-- Filtros para la tabla `valoracion_series`
--
ALTER TABLE `valoracion_series`
  ADD CONSTRAINT `valoracion_series_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `series` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
